
export default class KnownError extends Error {

  status = 0
  error:{ keys: string[], message: string }[]|null = null

  constructor({
    error, 
    message = 'ValidationError', 
    status = 422
  }:{
    error?: { key: string, message: string }[] 
    message?: string
    status?: number
  }) {

    super(message);
    this.name = "KnownError";
    this.status = status
    
    if(error) this.error = error.map(v => ({
      keys: [v.key],
      message: v.message
    }))
  }

}


