import { serve } from '@hono/node-server'
import app from './app'

const PORT = process.env.PORT ? Number(process.env.PORT) : 3333
console.log(`Running on ${PORT}`)

serve({
  fetch: app.fetch,
  port: PORT,
})

// update asdf

