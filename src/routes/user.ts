import { Hono } from 'hono'
import { z } from 'zod'
import { zValidator } from '@hono/zod-validator'
import { firestore } from '@/lib/firebase'
import {
  collection,
  query, where,
  doc,
  addDoc,
  getDoc,
  getDocs,
  updateDoc,
} from 'firebase/firestore';

import KnownError from '@/lib/known-error';

const user = new Hono()

user.get('/:id', async (c) => {
  
  const id = c.req.param('id')
  const d = await getDoc(doc(firestore, 'users', id))
  const data = d.data()

  if(!data?.name) throw new KnownError({
    status: 404,
    message: 'Not Found'
  })

  return c.json({ success: true, data: {
    id: d.id,
    name: data?.name,
    picture: data?.picture,
    username: data?.username
  } })

})

user.post('/',
  zValidator(
    'form',
    z.object({
      name: z.string().min(2).max(65535),
      picture: z.string().url().regex(
        /^.+\.(jpeg|jpg|webp|png|gif)$/i, 
        'Should ends with jpeg, jpg, png, webp, or gif'
      ),
      username: z.string().min(2).max(255)
    }).strict()
  ),
  async (c) => {

    
    let body = await c.req.parseBody()
    const userRef = collection(firestore, 'users')

    // get previous user with the same username
    const prevUserQ = query(userRef, where('username', '==', body.username))
    const snapshot = await getDocs(prevUserQ);

    // throw error if username already exists
    if(snapshot.size) throw new KnownError({
      status: 409,
      message: 'Conflict',
      error: [{
        key: 'username',
        message: 'username already taken'
      }]
    })

    // snapshot.forEach((doc) => {
    //   // doc.data() is never undefined for query doc snapshots
    //   console.log(doc.id, " => ", doc.data());
    // });
    

    const save = await addDoc(userRef, body)
    const d = await getDoc(doc(firestore, 'users', save.id))
    const data = d.data()

    return c.json({ success: true, data: {
      id: d.id,
      name: data?.name,
      picture: data?.picture,
      username: data?.username
    } })

  }
)

user.put('/:id',
  zValidator(
    'form',
    z.object({
      name: z.string().min(2).max(65535),
      picture: z.string().url().regex(
        /^.+\.(jpeg|jpg|webp|png|gif)$/i, 
        'Should ends with jpeg, jpg, png, webp, or gif'
      ),
      username: z.string().min(2).max(255)
    }).strict()
  ),
  async (c) => {

    const id = c.req.param('id')
    const body = await c.req.parseBody()

    // get previous user with the same username
    const prevUserQ = query(collection(firestore, 'users'), where('username', '==', body.username))
    const snapshot = await getDocs(prevUserQ);

    // throw error if existing user is not this user
    // this assumes usernames are unique
    if(snapshot.size) {
      let userId :string;
      snapshot.forEach((doc) => {
        userId = doc.id
        if(userId !== id) throw new KnownError({
          status: 409,
          message: 'Conflict',
          error: [{
            key: 'username',
            message: 'username already taken'
          }]
        })
      });
    }

    await updateDoc(doc(firestore, 'users', id), body)
    const d = await getDoc(doc(firestore, 'users', id))
    const data = d.data()

    return c.json({ success: true, data: {
      id: d.id,
      name: data?.name,
      picture: data?.picture,
      username: data?.username
    }})

  }
)

export default user