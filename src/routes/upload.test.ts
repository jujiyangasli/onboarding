import app from '@/app'
import { storage } from '@/lib/firebase'
import { ref, listAll, deleteObject } from "firebase/storage";
import { PREFIX } from './upload'

const getFormData = async () => {
  const url = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
  const blob = await fetch(url).then(res => res.blob())
  const fd = new FormData()
  fd.append('image', blob, 'filename.gif')
  return fd
}

async function deleteAllImage(){
  const listRef = ref(storage, PREFIX);
  return await listAll(listRef)
  .then((res) => {
    let deletion: any[] = []
    res.items.forEach((itemRef) => {
      deletion.push(deleteObject(itemRef))
    });
    return Promise.all(deletion)
  })
}

describe('upload', () => {

  beforeAll(async () => {
    return await deleteAllImage();
  },20000);

  afterAll(async () => {
    return await deleteAllImage();
  },20000);

  test('POST /upload', async () => {

    let body = await getFormData()
    const res = await app.request('/upload',{
      method: 'POST',
      body
    })

    expect(res.status).toBe(200)
    const { success, data } = await res.json()
    console.log(success, data)

  })

})
