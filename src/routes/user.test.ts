import app from '@/app'
import { firestore } from '@/lib/firebase'
import {
  collection,
  addDoc,
  getDocs,
  deleteDoc,
  doc,
} from 'firebase/firestore';

const users:{[key:string]: string} = {}

async function createUsers(){

  // delete all users
  await deleteUsers()

  console.log('creating 2 new users')

  // create users
  const userRef = collection(firestore, 'users')
  const user1 = await addDoc(userRef, {
    name: 'User1',
    picture: 'https://image.com/image.jpeg',
    username: 'user1'
  })

  const user2 = await addDoc(userRef, {
    name: 'User2',
    picture: 'https://image.com/image.jpeg',
    username: 'user2'
  })

  users.user1 = user1.id
  users.user2 = user2.id

}

async function deleteUsers(){

  // console.log('deleting all users')
  const querySnapshot = await getDocs(collection(firestore, "users"));
  let allData: any[] = []
  querySnapshot.forEach((document) => {
    allData.push(
      deleteDoc(doc(firestore, "users", document.id))
    )
  });
  await Promise.all(allData)

}

describe('users', () => {

  beforeAll(async () => {
    return await createUsers();
  },20000);

  afterAll(async () => {
    return await deleteUsers();
  },10000);

  test('POST /user', async () => {

    const res = await app
      .request('/user',{
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: new URLSearchParams({
          name: 'User',
          picture: 'https://image.com/image.jpeg',
          username: 'user'
        }).toString()
      })


    expect(res.status).toBe(200)
    const { success, data } = await res.json()

    expect(success).toBeTruthy()
    expect(data.id).toBeTruthy()
    expect(data.name).toBe('User')
    expect(data.picture).toBe('https://image.com/image.jpeg')
    expect(data.username).toBe('user')

  })

  test('PUT /user', async () => {

    const res = await app
      .request(`/user/${users.user1}`,{
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: new URLSearchParams({
          name: 'User1 Updated',
          picture: 'https://image.com/image.jpeg',
          username: 'user1updated'
        }).toString()
      })


    expect(res.status).toBe(200)
    const { success, data } = await res.json()

    expect(success).toBeTruthy()
    expect(data.id).toBe(users.user1)
    expect(data.name).toBe('User1 Updated')
    expect(data.picture).toBe('https://image.com/image.jpeg')
    expect(data.username).toBe('user1updated')

  })

  test('GET /user', async () => {

    const res = await app
      .request(`/user/${users.user2}`)


    expect(res.status).toBe(200)
    const { success, data } = await res.json()

    expect(success).toBeTruthy()
    expect(data.id).toBe(users.user2)
    expect(data.name).toBe('User2')
    expect(data.picture).toBe('https://image.com/image.jpeg')
    expect(data.username).toBe('user2')

  })
})