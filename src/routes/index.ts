import { Hono } from 'hono'
import user from './user'
import upload from './upload'
import KnownError from '@/lib/known-error'

const root = new Hono()

root.onError((e) => {

  if(!(e instanceof KnownError)){
    console.log(e)
  }

  let { status, error, message } = e instanceof KnownError ? e : {
    status: 500,
    message: 'ServerError',
    error: null
  }
  
  return new Response(JSON.stringify({
    status: false,
    message,
    error: error ? {
      issues: error,
      name: 'ValidationError'
    } : undefined
  }), {
    status: status,
    headers: {
      'Content-Type': 'application/json'
    },
  })

})

root.get('/', (c) => c.json({ hello: 'world' }))
root.route('/user', user)
root.route('/upload', upload)

export default root