import { Hono } from 'hono'
import { storage } from '@/lib/firebase'
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import KnownError from '@/lib/known-error';
import randomstring from 'randomstring'

import fs from 'fs'

export const PREFIX = 'images'

const upload = new Hono()

upload.post('/', async (c) => {

  const body = await c.req.parseBody()
  const image = body['image'] as File

  if(image.size > 5000000) throw new KnownError({
    status: 413,
    message: 'Request Entity Too Large'
  })

  if(!image.type.match(/^image\/(jpeg|jpg|webp|png|gif)$/i))
    throw new KnownError({
      status: 400,
      message: 'Should be a jpeg, jpg, png, webp, or gif'
    })
  
  const name = randomstring.generate() + image.type.split('/').pop()
  const imageRef = ref(storage, PREFIX + '/' + name);
  const snapshot = await uploadBytes(imageRef, await image.arrayBuffer(), {
    contentType: image.type
  })
  const downloadURL = await getDownloadURL(snapshot.ref);

  return c.json({ success: true, data: {
    image: downloadURL
  }})

})

export default upload