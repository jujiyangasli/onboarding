# Hello

This is a node.js API, written using [Hono](https://hono.dev/).
Deployed in render.com.

It uses Firebase for it's Firestore database and Storage.

## Dev

```bash
npm install
npm run start
```


open [http://localhost:3333](http://localhost:3333)


## Deployment
This is a nodejs app. It is deployed in Render.

Since it is a free version, it will need sometime to spin up on the initial request.
If you want to stress-test this, I recommend finding another server. AWS would work.


## Postman URL

[https://www.postman.com/juji/workspace/onboarding/overview](https://www.postman.com/juji/workspace/onboarding/overview)

You can set `host` in the Env Variable to:
```
https://onboarding-903f.onrender.com
```

It is the current dev version of the server.

This is an image on how to set the host in postman:
![How to add host](https://gcdnb.pbrd.co/images/tuRjYozp9kWo.png?o=1)
If the image above doesn't load: [click here](https://pasteboard.co/tuRjYozp9kWo.png)

## Testing

After installing the dependencies:
```bash
bun i
# or
yarn
# or
pnpm i
# or
npm i
```

Run the test on your local, or on any place really, using:
```bash
bun run test 
# or
yarn run test
# or
pnpm run test
# or
npm run test
```

It will wipe out the current database after test.

cheers,

[jujiyangasli](https://jujiyangasli.com)
